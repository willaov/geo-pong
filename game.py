#!./env/bin/python
import random
import json
import time
import argparse
from server import Region, Regions

parser = argparse.ArgumentParser(description='Run pong game')
parser.add_argument("region", help="choose game region to play in")
parser.add_argument("gamekey", help="unique game identifier")
parser.add_argument("-t", "--time", help="set edge-to-edge time in s", type=float)
parser.add_argument("-f", "--update-frequency", help="set update frequency", type=float)
parser.add_argument("-s", "--score", help="set winning score", type=float)


class DIRECTION:
    IDLE = 0
    UP = 1
    DOWN = 2
    LEFT = 3
    RIGHT = 4


class SECTOR:
    LEFT = 0
    RIGHT = 1


ySpeedReduction = 1.5
winningScore = 10

# Edge to edge time in s
timeEdgeToEdge = 300

# Game update frequency in Hz
updateFreq = 10


class Ball():
    def __init__(self, latlng, size, speed):
        self.width = size
        self.height = size/1.6
        self.x = latlng[1]
        self.y = latlng[0]
        self.moveX = DIRECTION.IDLE
        self.moveY = DIRECTION.IDLE
        self.speed = speed
        self.bounds = self._bounds

    @property
    def _bounds(self):
        self.bounds = [
            [self.y - self.height/2, self.x - self.width/2],
            [self.y + self.height/2, self.x + self.width/2]
        ]
        return self.bounds

    def setHeight(self, size):
        self.height = size/1.6
        self._bounds
        return self


class Paddle():
    def __init__(self, latlng, width, height, score=0):
        self.width = width
        self.height = height
        self.x = latlng[1]
        self.y = latlng[0]
        self.score = score
        self.bounds = self._bounds

    @property
    def _bounds(self):
        self.bounds = [
            [self.y - self.height/2, self.x - self.width/2],
            [self.y + self.height/2, self.x + self.width/2]
        ]
        return self.bounds

    def scaleY(self, scaling):
        self.height = self.height*scaling
        self._bounds
        return self

    def __repr__(self):
        return json.dumps({
            "width": self.width,
            "height": self.height,
            "x": self.x,
            "y": self.y,
            "score": self.score
        })


class Game():
    def __init__(self, region, gamekey):
        self.paddleHeight = region.height/10
        self.paddleWidth = self.paddleHeight/5
        self.ballSize = self.paddleHeight/2
        self.ballSpeed = region.width/(timeEdgeToEdge*updateFreq)
        self.offset = self.paddleHeight
        self.gamekey = gamekey

        if isinstance(region, Region):
            # Single region
            self.leftPlayer = Paddle([
                region.bEdge + region.height/2,
                region.lEdge + self.offset],
                self.paddleWidth, self.paddleHeight)
            self.rightPlayer = Paddle([
                region.bEdge + region.height/2,
                region.lEdge + region.width - self.offset],
                self.paddleWidth, self.paddleHeight)
            self.ball = Ball([
                region.bEdge + region.height/2,
                region.lEdge + region.width/2],
                self.ballSize, self.ballSpeed)

            self.leftEdge = region.lEdge
            self.rightEdge = region.lEdge + region.width
            self.topEdgeLeft = region.bEdge + region.height
            self.bottomEdgeLeft = region.bEdge
            self.topEdgeRight = self.topEdgeLeft
            self.bottomEdgeRight = self.bottomEdgeLeft
            self.width = region.width
            self.height = region.height
            self.region = region

            self.leftYScaling = 1

        elif isinstance(region, Regions):
            print("Multiple Regions")
            self.leftPlayer = Paddle([
                region.bEdgeLeft + region.lheight/2,
                region.lEdgeLeft + self.offset],
                self.paddleWidth, self.paddleHeight*region.leftYScaling)
            self.rightPlayer = Paddle([
                region.bEdgeRight + region.height/2,
                region.lEdgeRight + region.width/2 - self.offset],
                self.paddleWidth, self.paddleHeight)
            self.ball = Ball([
                region.bEdgeRight + region.height/2,
                region.lEdgeRight + region.width/4],
                self.ballSize, self.ballSpeed)

            self.leftEdge = region.lEdgeLeft
            self.rightEdge = region.lEdgeRight + region.width/2
            self.topEdgeLeft = region.bEdgeLeft + region.lheight
            self.bottomEdgeLeft = region.bEdgeLeft
            self.topEdgeRight = region.bEdgeRight + region.height
            self.bottomEdgeRight = region.bEdgeRight
            self.width = region.width
            self.height = region.height
            self.lheight = region.lheight
            self.region = region
            self.leftYScaling = region.leftYScaling

            self.xwarp = region.lEdgeRight - region.lEdgeLeft - region.width/2
            self.ywarp = region.bEdgeRight - region.bEdgeLeft/region.leftYScaling
        else:
            raise TypeError("The game region must be a Region or Regions class")

        self.running = False
        self.over = False
        self.turn = self.rightPlayer
        self.timer = 0
        self.round = 0

    @property
    def ballSector(self):
        # Set ball sector, and scale height accordingly
        if self.ball.x <= (self.leftEdge + self.rightEdge)/2:
            self.ball.setHeight(self.ballSize*self.leftYScaling)
            return SECTOR.LEFT
        else:
            self.ball.setHeight(self.ballSize)
            return SECTOR.RIGHT

    def update(self):
        try:
            leftPaddle = json.load(open(self.gamekey + "/leftPaddle.json"))
            self.leftPlayer = Paddle([
                leftPaddle["lat"],
                leftPaddle["lng"]
            ], self.paddleWidth, self.paddleHeight*self.leftYScaling, self.leftPlayer.score)
        except (FileNotFoundError, KeyError, json.decoder.JSONDecodeError, AttributeError):
            pass
        try:
            rightPaddle = json.load(open(self.gamekey + "/rightPaddle.json"))
            self.rightPlayer = Paddle([
                rightPaddle["lat"],
                rightPaddle["lng"]
            ], self.paddleWidth, self.paddleHeight, self.rightPlayer.score)
        except (FileNotFoundError, KeyError, json.decoder.JSONDecodeError, AttributeError):
            pass

        if not self.over:
            # Ball collisions with boundary
            if self.ball.x - self.ball.width/2 <= self.leftEdge:
                self.resetTurn(self.rightPlayer, self.leftPlayer)
            if self.ball.x + self.ball.width/2 >= self.rightEdge:
                self.resetTurn(self.leftPlayer, self.rightPlayer)
            if self.ballSector == SECTOR.LEFT:
                if self.ball.y - self.ball.height/2 <= self.bottomEdgeLeft:
                    self.ball.moveY = DIRECTION.UP
                if self.ball.y + self.ball.height/2 >= self.topEdgeLeft:
                    self.ball.moveY = DIRECTION.DOWN
            elif self.ballSector == SECTOR.RIGHT:
                if self.ball.y - self.ball.height/2 <= self.bottomEdgeRight:
                    self.ball.moveY = DIRECTION.UP
                if self.ball.y + self.ball.height/2 >= self.topEdgeRight:
                    self.ball.moveY = DIRECTION.DOWN
            else:
                print("Ball not in any sector")

            # On new serve randomise ball direction
            if self.turnDelayIsOver and self.turn is not None:
                self.ballSector
                self.ball.moveX = DIRECTION.LEFT if self.turn == self.rightPlayer else DIRECTION.RIGHT   # noqa
                self.ball.moveY = [DIRECTION.UP, DIRECTION.DOWN][round(random.random())]
                self.ball.x = self.rightEdge - self.width/2
                self.ball.y = random.random()*(self.height-self.ball.height) + self.bottomEdgeRight + self.ball.height/2
                self.turn = None

            # Move ball
            if self.ballSector == SECTOR.LEFT:
                ySpeed = self.ball.speed*self.leftYScaling
            else:
                ySpeed = self.ball.speed
            if self.ball.moveY == DIRECTION.UP:
                self.ball.y += ySpeed / ySpeedReduction
            elif self.ball.moveY == DIRECTION.DOWN:
                self.ball.y -= ySpeed / ySpeedReduction
            if self.ball.moveX == DIRECTION.LEFT:
                self.ball.x -= self.ball.speed
            elif self.ball.moveX == DIRECTION.RIGHT:
                self.ball.x += self.ball.speed

            # Warp ball through inter-sector space if necessary
            if isinstance(self.region, Regions):
                if self.ballSector == SECTOR.LEFT and self.ball.x > self.leftEdge + self.width/2:
                    # Warp ball right
                    self.ball.x += self.xwarp
                    self.ball.y = self.ball.y/self.leftYScaling + self.ywarp
                elif self.ballSector == SECTOR.RIGHT and self.ball.x < self.rightEdge - self.width/2:
                    # Warp ball left
                    self.ball.x -= self.xwarp
                    self.ball.y = (self.ball.y - self.ywarp)*self.leftYScaling

            # Handle leftPlayer-ball collisions
            if self.ball.x - self.ball.width/2 <= self.leftPlayer.x + self.leftPlayer.width/2\
                    and self.ball.x + self.ball.width/2 >= self.leftPlayer.x - self.leftPlayer.width/2:
                if self.ball.y - self.ball.height/2 <= self.leftPlayer.y + self.leftPlayer.height/2\
                        and self.ball.y + self.ball.height/2 >= self.leftPlayer.y - self.leftPlayer.height/2:
                    self.ball.x = self.leftPlayer.x + self.ball.width/2 + self.leftPlayer.width/2
                    self.ball.moveX = DIRECTION.RIGHT

            # Handle rightPlayer-ball collisions
            if self.ball.x - self.ball.width/2 <= self.rightPlayer.x + self.rightPlayer.width/2\
                    and self.ball.x + self.ball.width/2 >= self.rightPlayer.x - self.rightPlayer.width/2:
                if self.ball.y - self.ball.height/2 <= self.rightPlayer.y + self.rightPlayer.height/2\
                        and self.ball.y + self.ball.height/2 >= self.rightPlayer.y - self.rightPlayer.height/2:
                    self.ball.x = self.rightPlayer.x - self.ball.width/2 - self.rightPlayer.width/2
                    self.ball.moveX = DIRECTION.LEFT

            # Update Bounds
            self.leftPlayer._bounds
            self.rightPlayer._bounds
            self.ball._bounds

        if self.leftPlayer.score >= winningScore:
            self.over = True
            return "Left Win"

        elif self.rightPlayer.score >= winningScore:
            self.over = True
            return "Right Win"

    def resetTurn(self, victor, loser):
        self.turn = loser
        victor.score += 1

    def turnDelayIsOver(self):
        return True

    def __repr__(self):
        return ("Left Score: {leftscore}\n"
                "Right Score: {rightscore}\n").format(
                    leftscore=self.leftPlayer.score,
                    rightscore=self.rightPlayer.score)

    def saveState(self):
        try:
            with open(self.gamekey + "/gameState.json", "w") as fp:
                json.dump({
                    "leftPlayer": vars(self.leftPlayer),
                    "rightPlayer": vars(self.rightPlayer),
                    "ball": vars(self.ball),
                    "over": self.over
                }, fp)
        except FileNotFoundError:
            os.mkdir(self.gamekey)
            with open(self.gamekey + "/gameState.json", "w") as fp:
                json.dump({
                    "leftPlayer": vars(self.leftPlayer),
                    "rightPlayer": vars(self.rightPlayer),
                    "ball": vars(self.ball),
                    "over": self.over
                }, fp)


if __name__ == "__main__":
    from server import gameRegions
    import os

    region = None

    args = parser.parse_args()

    try:
        region = gameRegions[args.region]
        location = args.region
    except KeyError:
        print("Defaulting to queens region")
        region = gameRegions["queens"]
        location = "queens"

    gamekey = args.gamekey

    if args.time:
        print("Set Time to {}".format(args.time))
        timeEdgeToEdge = args.time

    if args.update_frequency:
        print("Set Update Frequency to {}".format(args.update_frequency))
        updateFreq = args.update_frequency

    if args.score:
        print("Set Winning Score to {}".format(args.score))
        winningScore = args.score

    game = Game(region, gamekey)

    try:
        os.remove(gamekey + "/leftPaddle.json")
    except FileNotFoundError:
        pass
    try:
        os.remove(gamekey + "/rightPaddle.json")
    except FileNotFoundError:
        pass

    try:
        open(gamekey + "/" + location + ".json", "w")
    except FileNotFoundError:
        os.mkdir(gamekey)
        open(gamekey + "/" + location + ".json", "w")

    gameStr = ""
    while 1:
        oldGameStr = gameStr
        gameStr = str(game)
        if oldGameStr != gameStr:
            print(game)
        res = game.update()
        game.saveState()
        time.sleep(1/updateFreq)
        if res is not None:
            print(game)
            print(res)
            break
