from os import walk
import json

f = []
for (dirpath, dirnames, filenames) in walk("./"):
    f.extend(filenames)
    break

for filename in f:
    if ".json" in filename:
        data = json.load(open(filename))
        try:
            assert data["location"] + ".json" == filename
            print("Verified", filename)
        except AssertionError:
            msg = "Verification Failed - Location: {}, Filename: {}".format(data["location"], filename)
            print(msg)
            raise ValueError(msg)
