var myaccessToken = "pk.eyJ1Ijoid2lsbGFvdiIsImEiOiJjazYwd21vNjcwYnFkM25ueTB3dXBuOHAwIn0.shHDpzplHEF1sktnlDWLxw";
var accessToken = "pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw";
var controlKey = null;
var takingControl = false;

var updateFrequency = 5;
var pollFrequency = 1;

// Create Map
var map = L.map('map', {
    center: centerPoint,
    zoom: zoomLevel,
    zoomControl: false,
    boxZoom: false,
    doubleClickZoom: false,
    dragging: false,
    touchZoom: false,
    scrollWheelZoom: false,
    keyboard: false
});

// Add map tiles
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxBounds: bounds,
    autoPan: false,
    zoomSnap: 0,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: accessToken
}).addTo(map);

// Draw bounds
var playPolygon = L.polygon(playRegion, {fill: false}).addTo(map);

// Draw center line
var centerLine = L.polyline([
    [bounds[1][0], centerPoint[1]],
    [bounds[0][0], centerPoint[1]]
], {
    dashArray: "4"
}).addTo(map)

// Draw score display
var scoreCircleRad = Math.abs((bounds[1][0]-bounds[0][0])*2*Math.pow(10, 1));
var scoreCircleOffset = (bounds[1][0]-bounds[0][0])/9
var scoreDisplayPaddle = L.circle([bounds[0][0] - scoreCircleOffset, centerPoint[1]], scoreCircleRad).addTo(map)

var scoreDisplay = scoreDisplayPaddle.bindTooltip("Score: ",
    {permanent: true, direction:"center"}
).openTooltip()

// Draw location control grabber
var circleRad = Math.abs((bounds[1][0]-bounds[0][0])*2*Math.pow(10, 4));
var circleOffset = (bounds[1][0]-bounds[0][0])/4
var controlPaddle = L.circle([bounds[1][0] + circleOffset, centerPoint[1]], circleRad).addTo(map)

var infoText = controlPaddle.bindTooltip("Click this circle to control the paddle",
    {permanent: true, direction:"center"}
).openTooltip()

// Draw paddles and ball
var leftPaddle = L.rectangle([[0, 0], [0, 0]], {
    color: "#002147",
    weight: 1,
    fillOpacity: 1
}).addTo(map)

var rightPaddle = L.rectangle([[0, 0], [0, 0]], {
    color: "#a3c1ad",
    weight: 1,
    fillOpacity: 1
}).addTo(map)

var ball = L.rectangle([[0, 0], [0, 0]], {
    color: "#ff7800",
    weight: 1,
    fillOpacity: 1
}).addTo(map)

// Size map
var group = new L.featureGroup([playPolygon, controlPaddle, scoreDisplayPaddle]);
map.fitBounds(group.getBounds(), {animate: false});
