var myaccessToken = "pk.eyJ1Ijoid2lsbGFvdiIsImEiOiJjazYwd21vNjcwYnFkM25ueTB3dXBuOHAwIn0.shHDpzplHEF1sktnlDWLxw";
var accessToken = "pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw";
var controlKey = null;
var takingControl = false;

var updateFrequency = 5;
var pollFrequency = 1;

var leftBounds = [bounds[0], [bounds[1][0], centerPoint[1]]]
var rightBounds = [[bounds[0][0], centerPoint[1]], bounds[1]]

// Create Map
var map = L.map('map', {
    center: centerPoint,
    zoom: zoomLevel,
    zoomControl: false,
    boxZoom: false,
    doubleClickZoom: false,
    dragging: false,
    touchZoom: false,
    scrollWheelZoom: false,
    keyboard: false
});

// Add map tiles
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxBounds: bounds,
    autoPan: false,
    zoomSnap: 0,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: accessToken
}).addTo(map);

// Draw bounds
var leftPolygon = L.rectangle(leftBounds).addTo(map);
var rightPolygon = L.rectangle(rightBounds).addTo(map);

leftPolygon.bindTooltip("Click here for info", {permanent: true, direction:"center"});
rightPolygon.bindTooltip("Click here to view locations", {permanent: true, direction:"center"});

// Draw info display
var info = 'To play this game, navigate to the game url you were sent,\
            eg <a>waov2.user.srcf.net/geo-pong/cambridge/aBcDeFgHiJ/</a><br><br>\
            To view a location map go to <a>waov2.user.srcf.net/geo-pong/<i>location</i>/</a><br><br>\
            To start a game navigate to <a>waov2.user.srcf.net/geo-pong/<i>location</i>/startgame/</a><br><br>\
            To create a new location to play go to <a>waov2.user.srcf.net/geo-pong/addone/<i>location</i>/</a><br><br>\
            To create a new two-map location (see oxbridge for an example) go to <a>waov2.user.srcf.net/geo-pong/addtwo/<i>location</i>/</a>';

leftPolygon.bindPopup(info);

var form = `<form onsubmit="return officialForm();" id="officialForm">\
            <label for="official">Official Game Locations</label>\
            ${officialGameSelect}\
            <input type="submit" value="View Map">\
            </form><br>\
            <form onsubmit="return allForm();" id="allForm">\
            <label for="unofficial">All Game Locations</label>\
            ${allGameSelect}\
            <input type="submit" value="View Map">\
            </form><br>\
            <form onsubmit="return newForm();" id="newForm">\
            <label for="location">Add New Location</label>\
            <input type="text" id="location" name="location" placeholder="New Location Name"><br>\
            <label for="oneMap">One&nbspMap</label>\
            <input type="radio" id="oneMap" name="num" value="addone" checked>\
            <label for="twoMaps">Two&nbspMaps</label>\
            <input type="radio" id="twoMaps" name="num" value="addtwo">\
            <input type="submit" value="Create Map">\
            </form>`

rightPolygon.bindPopup(form);

// Size map
map.fitBounds(bounds, {animate: false});

function officialForm() {
    var formEl = document.forms.officialForm;
    var formData = new FormData(formEl);
    var game = formData.get('game');
    window.location.href = game;
    return false;
}

function allForm() {
    var formEl = document.forms.allForm;
    var formData = new FormData(formEl);
    var game = formData.get('game');
    window.location.href = game;
    return false;
}

function newForm() {
    var formEl = document.forms.newForm;
    var formData = new FormData(formEl);
    var location = formData.get('location');
    var addNum = formData.get('num');
    if (location.toString().indexOf(' ') >= 0) {
        alert("Place name cannot include spaces");
        window.location.reload();
        return false;
    } else {
        window.location.href = addNum + "/" + location;
        return false;
    }
}
