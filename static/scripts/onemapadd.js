var myaccessToken = "pk.eyJ1Ijoid2lsbGFvdiIsImEiOiJjazYwd21vNjcwYnFkM25ueTB3dXBuOHAwIn0.shHDpzplHEF1sktnlDWLxw";
var accessToken = "pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw";

// Create Map
var map = L.map('halfmaptop', {
    center: [0, 0],
    zoom: 1,
    zoomControl: false,
    doubleClickZoom: false
});

// Add map tiles
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    autoPan: false,
    zoomSnap: 0,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: accessToken
}).addTo(map);

// Draw polygon
var playRectangle = L.rectangle([[0,0],[0,0]], {fill: false}).addTo(map);
console.log(playRectangle.getBounds())

// Set form values
function setForm() {
    var bEdge = document.getElementById("bEdge");
    var lEdge = document.getElementById("lEdge");
    var height = document.getElementById("height");
    var width = document.getElementById("width");
    bEdge.value = playRectangle.getBounds().getSouth();
    lEdge.value = playRectangle.getBounds().getWest();
    height.value = playRectangle.getBounds().getNorth() - playRectangle.getBounds().getSouth();
    width.value = playRectangle.getBounds().getEast() - playRectangle.getBounds().getWest();
}

// Set polygon coords
function onMapLeftClick(e) {
    var ne = playRectangle.getBounds().getNorthEast();
    var bounds = [e.latlng, ne];
    playRectangle.setBounds(bounds);
    if (playRectangle.getBounds().getSouth() < e.latlng.lat || playRectangle.getBounds().getWest() < e.latlng.lng) {
        playRectangle.setBounds([e.latlng, e.latlng]);
    }
    setForm();
}

function onMapRightClick(e) {
    var sw = playRectangle.getBounds().getSouthWest();
    var bounds = [sw, e.latlng];
    playRectangle.setBounds(bounds);
    if (playRectangle.getBounds().getNorth() > e.latlng.lat || playRectangle.getBounds().getEast() > e.latlng.lng) {
        playRectangle.setBounds([e.latlng, e.latlng]);
    }
    setForm();
}

map.on('dblclick', onMapLeftClick);
map.on('contextmenu', onMapRightClick);

// Center map on person
map.locate({setView: true, maxZoom: 16});
function onLocationError(e) {
    alert(e.message);
}
map.on('locationerror', onLocationError);