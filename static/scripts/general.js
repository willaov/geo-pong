var getJSON = function(url, callback) {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    
    xhr.onload = function() {
    
        var status = xhr.status;
        
        if (status == 200) {
            callback(null, xhr.response);
        } else {
            callback(status);
        }
    };
    
    xhr.send();
};

var getText = function(url, callback) {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'text';
    
    xhr.onload = function() {
    
        var status = xhr.status;
        
        if (status == 200) {
            callback(null, xhr.responseText);
        } else {
            callback(status);
        }
    };
    
    xhr.send();
};

var sendJSON = function(url, data) {

    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    
    xhr.onload = function() {
    
        var status = xhr.status;
        
        if (status == 200) {
        } else {
            console.log(status)
        }
    };

    xhr.setRequestHeader("Content-type", "application/json");
    
    xhr.send(JSON.stringify(data));
}

var sendJSONVerify = function(url, data) {

    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.responseType = 'text';
    
    xhr.onload = function() {
    
        var status = xhr.status;
        
        if (status == 200) {
            if (xhr.responseText === "Success") {
            } else {
                if (data.key === null) {
                } else {
                    controlKey = null
                    alert("Somebody else has taken control")
                }
            }
        } else {
            console.log(status)
        }
    };

    xhr.setRequestHeader("Content-type", "application/json");
    
    xhr.send(JSON.stringify(data));
}

function startNewGame() {
    window.location.href = "startgame"
}