// Control everything
function movePaddle(e) {
    if (controlKey === null && takingControl) {
        if (e.latlng.lng <= centerPoint[1]) {
            if (confirm("Do you want to take control of the West paddle?")) {
                getText('controlLeftPaddle/',  function(err, data) {
                    if (err != null) {
                        console.error(err);
                    } else {
                        controlKey = data
                        console.log(controlKey)
                    }
                });
                sendData = e.latlng
                sendData.key = controlKey
                sendJSONVerify('moveLeftPaddle/', sendData);
            }
            else {
                alert("You can only control the paddle for the half you're in")
            }
        }
        else {
            if (confirm("Do you want to take control of the East paddle?")) {
                getText('controlRightPaddle/',  function(err, data) {
                    if (err != null) {
                        console.error(err);
                    } else {
                        controlKey = data
                        console.log(controlKey)
                    }
                });
                sendData = e.latlng
                sendData.key = controlKey
                sendJSONVerify('moveRightPaddle/', sendData);
            }
            else {
                alert("You can only control the paddle for the half you're in")
            }
        }
    }
    else {
        if (e.latlng.lng <= centerPoint[1]) {
            sendData = e.latlng
            sendData.key = controlKey
            sendJSONVerify('moveLeftPaddle/', sendData);
        }
        else {
            sendData = e.latlng
            sendData.key = controlKey
            sendJSONVerify('moveRightPaddle/', sendData);
        }
    }
}

function onMapClick(e) {
    takingControl = true;
    movePaddle(e);
}

// Number of updated since game end
var endCount = 0;

function onLocateClickLeft() {
    if (endCount > 1) {
        if (confirm("Start new game?")) {
            startNewGame();
        }
    }
    takingControl = true
    leftmap.locate();
}
function onLocateClickRight() {
    if (endCount > 1) {
        if (confirm("Start new game?")) {
            startNewGame();
        }
    }
    takingControl = true
    rightmap.locate();
}

function locateUpdateLeft() {
    try {
        leftmap.locate();
    }
    catch (e) {
        console.err(e);
        setTimeout(locateUpdateLeft, 1000/pollFrequency);
    }
}
function locateUpdateRight() {
    try {
        rightmap.locate();
    }
    catch (e) {
        console.err(e);
        setTimeout(locateUpdateRight, 1000/pollFrequency);
    }
}

function onLocationFoundLeft(e) {
    console.log("Moving to location")
    movePaddle(e);
    if (controlKey !== null || takingControl === true) {
        setTimeout(locateUpdateLeft, 1000/pollFrequency);
    }
    takingControl = false;
}
function onLocationFoundRight(e) {
    console.log("Moving to location")
    movePaddle(e);
    if (controlKey !== null || takingControl === true) {
        setTimeout(locateUpdateRight, 1000/pollFrequency);
    }
    takingControl = false;
}

leftmap.on('locationfound', onLocationFoundLeft);
rightmap.on('locationfound', onLocationFoundRight);

function onLocationError(e) {
    alert(e.message);
}

leftmap.on('locationerror', onLocationError);
rightmap.on('locationerror', onLocationError);

function updateGame() {
    getJSON('gamestate/',  function(err, data) {

        if (err != null) {
            console.error(err);
        } else {
            // Move all items to current location
            leftPaddle.setBounds(data.leftPlayer.bounds);
            rightPaddle.setBounds(data.rightPlayer.bounds);
            // Ball Trails
            if (ballTrails) {
                ballLeft = L.rectangle(data.ball.bounds, {
                    color: "#ff7800",
                    weight: 1,
                    fillOpacity: 1
                }).addTo(leftmap)
                ballRight = L.rectangle(data.ball.bounds, {
                    color: "#ff7800",
                    weight: 1,
                    fillOpacity: 1
                }).addTo(rightmap)
            }
            else {
                ballLeft.setBounds(data.ball.bounds);
                ballRight.setBounds(data.ball.bounds);
            }
            if (!data.over) {
                scoreDisplayLeft.setTooltipContent(`Score: ${data.leftPlayer.score}`);
                scoreDisplayRight.setTooltipContent(`Score: ${data.rightPlayer.score}`);
                endCount = 0;
            }
            else {
                scoreDisplayLeft.setTooltipContent(`You ${data.leftPlayer.score > data.rightPlayer.score ? "Won" : "Lost"}<br>\
                                                    Click the circle at the top to play again`);
                scoreDisplayRight.setTooltipContent(`You ${data.leftPlayer.score > data.rightPlayer.score ? "Lost" : "Won"}<br>\
                                                    Click the circle at the top to play again`);
                endCount++;
            }
        }
    });
}

setInterval(updateGame, 1000/updateFrequency)

controlPaddleLeft.on('click', onLocateClickLeft);
controlPaddleRight.on('click', onLocateClickRight);
