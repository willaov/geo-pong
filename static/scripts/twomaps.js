var myaccessToken = "pk.eyJ1Ijoid2lsbGFvdiIsImEiOiJjazYwd21vNjcwYnFkM25ueTB3dXBuOHAwIn0.shHDpzplHEF1sktnlDWLxw";
var accessToken = "pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw";
var controlKey = null;
var takingControl = false;

var updateFrequency = 5;
var pollFrequency = 1;

// Overall center point
var centerPoint = [(centerPointLeft[0]+centerPointRight[0])/2, (centerPointLeft[1]+centerPointRight[1])/2]

// Create Map
var leftmap = L.map('halfmapleft', {
    center: centerPointLeft,
    zoom: zoomLevel,
    zoomControl: false,
    boxZoom: false,
    doubleClickZoom: false,
    dragging: false,
    touchZoom: false,
    scrollWheelZoom: false,
    keyboard: false
});
var rightmap = L.map('halfmapright', {
    center: centerPointRight,
    zoom: zoomLevel,
    zoomControl: false,
    boxZoom: false,
    doubleClickZoom: false,
    dragging: false,
    touchZoom: false,
    scrollWheelZoom: false,
    keyboard: false
});

// Add map tiles
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxBounds: boundsLeft,
    autoPan: false,
    zoomSnap: 0,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: accessToken
}).addTo(leftmap);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxBounds: boundsRight,
    autoPan: false,
    zoomSnap: 0,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: accessToken
}).addTo(rightmap);

// Draw bounds
var playPolygonLeft = L.polygon(playRegionLeft, {fill: false}).addTo(leftmap);
var playPolygonRight = L.polygon(playRegionRight, {fill: false}).addTo(rightmap);

// Draw score displays
var scoreCircleRad = Math.abs((boundsLeft[1][0]-boundsLeft[0][0])*2*Math.pow(10, 1));
var scoreCircleOffset = (boundsLeft[1][0]-boundsLeft[0][0])/9

var scoreDisplayPaddleLeft = L.circle([boundsLeft[0][0] - scoreCircleOffset, centerPointLeft[1]], scoreCircleRad).addTo(leftmap)
var scoreDisplayPaddleRight = L.circle([boundsRight[0][0] - scoreCircleOffset, centerPointRight[1]], scoreCircleRad).addTo(rightmap)

var scoreDisplayLeft = scoreDisplayPaddleLeft.bindTooltip("Score: ",
    {permanent: true, direction:"center"}
).openTooltip()
var scoreDisplayRight = scoreDisplayPaddleRight.bindTooltip("Score: ",
    {permanent: true, direction:"center"}
).openTooltip()

// Draw location control grabber
var circleRadLeft = Math.abs((boundsLeft[1][0]-boundsLeft[0][0])*2*Math.pow(10, 4));
var circleOffsetLeft = (boundsLeft[1][0]-boundsLeft[0][0])/4;
var controlPaddleLeft = L.circle([boundsLeft[1][0] + circleOffsetLeft, centerPointLeft[1]], circleRadLeft).addTo(leftmap);

var circleRadRight = Math.abs((boundsRight[1][0]-boundsRight[0][0])*2*Math.pow(10, 4));
var circleOffsetRight = (boundsRight[1][0]-boundsRight[0][0])/4;
var controlPaddleRight = L.circle([boundsRight[1][0] + circleOffsetRight, centerPointRight[1]], circleRadRight).addTo(rightmap);

var infoTextLeft = controlPaddleLeft.bindTooltip("Click this circle to control the paddle",
    {permanent: true, direction:"center"}
).openTooltip()
var infoTextRight = controlPaddleRight.bindTooltip("Click this circle to control the paddle",
    {permanent: true, direction:"center"}
).openTooltip()

// Draw paddles and ball
var leftPaddle = L.rectangle([[0, 0], [0, 0]], {
    color: "#002147",
    weight: 1,
    fillOpacity: 1
}).addTo(leftmap)

var rightPaddle = L.rectangle([[0, 0], [0, 0]], {
    color: "#a3c1ad",
    weight: 1,
    fillOpacity: 1
}).addTo(rightmap)

var ballLeft = L.rectangle([[0, 0], [0, 0]], {
    color: "#ff7800",
    weight: 1,
    fillOpacity: 1
}).addTo(leftmap)
var ballRight = L.rectangle([[0, 0], [0, 0]], {
    color: "#ff7800",
    weight: 1,
    fillOpacity: 1
}).addTo(rightmap)

// Size map
var group = new L.featureGroup([playPolygonLeft, controlPaddleLeft]);
leftmap.fitBounds(group.getBounds(), {animate: false});
var group = new L.featureGroup([playPolygonRight, controlPaddleRight]);
rightmap.fitBounds(group.getBounds(), {animate: false});

// Move halves together
function panLeft() {
    // Get box edge
    var leftside = document.getElementById("halfmapleft")
    var overlay = leftside.getElementsByClassName("leaflet-overlay-pane")[0];
    var path = overlay.getElementsByTagName("path");
    var element = path[0];
    var domRect = element.getBoundingClientRect();
    // Get center edge
    var domRectCenter = leftside.getBoundingClientRect();
    console.log("Panning");
    leftmap.panBy([domRect.right-domRectCenter.right, 0], {animate: false});
}
panLeft();

function panRight() {
    // Get box edge
    var rightside = document.getElementById("halfmapright")
    var overlay = rightside.getElementsByClassName("leaflet-overlay-pane")[0];
    var path = overlay.getElementsByTagName("path");
    var element = path[0];
    var domRect = element.getBoundingClientRect();
    // Get center edge
    var domRectCenter = rightside.getBoundingClientRect();
    console.log("Panning");
    rightmap.panBy([domRect.left-domRectCenter.left, 0], {animate: false});
    rightmap.setZoom(leftmap.getZoom())
}
panRight();
