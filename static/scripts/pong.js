var DIRECTION = {
    IDLE: 0,
    UP: 1,
    DOWN: 2,
    LEFT: 3,
    RIGHT: 4
}

var rounds = [5, 5, 3, 3, 2];
var colors = ['#1abc9c', '#2ecc71', '#3498db', '#e74c3c', '#9b59b6'];

var ballSize = 0.005;
var paddleWidth = 0.005;
var paddleHeight = 0.01;
var offset = 0.005;
var paddleSpeed = 0.001;
var aiSpeed = 0.0001;
var ballSpeed = 0.001;

var gameWidth = Math.abs(tlcorner[1] - brcorner[1]);
var gameLeft = tlcorner[1] + offset;
var edgeLeft = tlcorner[1];
var gameRight = brcorner[1] - offset;
var edgeRight = brcorner[1];
var gameHeight = Math.abs(tlcorner[0] - brcorner[0]);
var edgeBottom = brcorner[0];
var edgeTop = tlcorner[0];


var Ball = {
    new: function (incrementedSpeed) {
        return {
            width: ballSize,
            height: ballSize,
            x: (gameWidth/2) - (ballSize/2),
            y: (gameHeight/2) - (ballSize/2),
            moveX: DIRECTION.IDLE,
            moveY: DIRECTION.IDLE,
            speed: incrementedSpeed || ballSpeed
        };
    }
};


var Paddle = {
    new: function (side) {
        return {
			width: paddleWidth,
			height: paddleHeight,
			x: side === 'left' ? gameLeft : gameRight,
			y: (gameHeight/2) - (paddleHeight/2),
			score: 0,
			move: DIRECTION.IDLE,
			speed: paddleSpeed
		};
    }
}


var Game = {
	initialize: function (map) {
        this.map = map
        this.player = Paddle.new.call(this, 'left');
		this.paddle = Paddle.new.call(this, 'right');
		this.ball = Ball.new.call(this);

		this.paddle.speed = aiSpeed;
		this.running = this.over = false;
		this.turn = this.paddle;
		this.timer = this.round = 0;
		this.color = '#2c3e50';

		Pong.listen();
	},

	// Update all objects (move the player, paddle, ball, increment the score, etc.)
	update: function () {
		if (!this.over) {
			// If the ball collides with the bound limits - correct the x and y coords.
			if (this.ball.x <= edgeLeft) Pong._resetTurn.call(this, this.paddle, this.player);
			if (this.ball.x >= edgeRight - this.ball.width) Pong._resetTurn.call(this, this.player, this.paddle);
			if (this.ball.y <= edgeBottom) this.ball.moveY = DIRECTION.DOWN;
			if (this.ball.y >= edgeTop - this.ball.height) this.ball.moveY = DIRECTION.UP;

			// Move player if they player.move value was updated by a keyboard event
			if (this.player.move === DIRECTION.UP) this.player.y += this.player.speed;
			else if (this.player.move === DIRECTION.DOWN) this.player.y -= this.player.speed;

			// On new serve (start of each turn) move the ball to the correct side
			// and randomize the direction to add some challenge.
			if (Pong._turnDelayIsOver.call(this) && this.turn) {
				this.ball.moveX = this.turn === this.player ? DIRECTION.LEFT : DIRECTION.RIGHT;
				this.ball.moveY = [DIRECTION.UP, DIRECTION.DOWN][Math.round(Math.random())];
				this.ball.y = Math.random() * gameHeight + edgeBottom;
				this.turn = null;
			}

			// If the player collides with the bound limits, update the x and y coords.
			if (this.player.y <= edgeBottom) this.player.y = edgeBottom;
			else if (this.player.y >= (edgeTop - this.player.height)) this.player.y = (edgeTop - this.player.height);

			// Move ball in intended direction based on moveY and moveX values
			if (this.ball.moveY === DIRECTION.UP) this.ball.y -= (this.ball.speed / 1.5);
			else if (this.ball.moveY === DIRECTION.DOWN) this.ball.y += (this.ball.speed / 1.5);
			if (this.ball.moveX === DIRECTION.LEFT) this.ball.x -= this.ball.speed;
			else if (this.ball.moveX === DIRECTION.RIGHT) this.ball.x += this.ball.speed;

			// Handle paddle (AI) UP and DOWN movement
			if (this.paddle.y > this.ball.y - (this.paddle.height / 2)) {
				if (this.ball.moveX === DIRECTION.RIGHT) this.paddle.y -= this.paddle.speed / 1.5;
				else this.paddle.y -= this.paddle.speed / 4;
			}
			if (this.paddle.y < this.ball.y - (this.paddle.height / 2)) {
				if (this.ball.moveX === DIRECTION.RIGHT) this.paddle.y += this.paddle.speed / 1.5;
				else this.paddle.y += this.paddle.speed / 4;
			}

			// Handle paddle (AI) wall collision
			if (this.paddle.y >= edgeTop - this.paddle.height) this.paddle.y = edgeTop - this.paddle.height;
			else if (this.paddle.y <= edgeBottom) this.paddle.y = edgeBottom;

			// Handle Player-Ball collisions
			if (this.ball.x - this.ball.width <= this.player.x && this.ball.x >= this.player.x - this.player.width) {
				if (this.ball.y <= this.player.y + this.player.height && this.ball.y + this.ball.height >= this.player.y) {
					this.ball.x = (this.player.x + this.ball.width);
					this.ball.moveX = DIRECTION.RIGHT;
				}
			}

			// Handle paddle-ball collision
			if (this.ball.x - this.ball.width <= this.paddle.x && this.ball.x >= this.paddle.x - this.paddle.width) {
				if (this.ball.y <= this.paddle.y + this.paddle.height && this.ball.y + this.ball.height >= this.paddle.y) {
					this.ball.x = (this.paddle.x - this.ball.width);
					this.ball.moveX = DIRECTION.LEFT;
				}
			}
		}

		// Handle the end of round transition
		// Check to see if the player won the round.
		if (this.player.score === rounds[this.round]) {
			// Check to see if there are any more rounds/levels left and display the victory screen if
			// there are not.
			if (!rounds[this.round + 1]) {
				this.over = true;
				setTimeout(function () { alert('Winner!'); }, 1000);
			} else {
				// If there is another round, reset all the values and increment the round number.
				this.color = this._generateRoundColor();
				this.player.score = this.paddle.score = 0;
				this.player.speed += 0.5;
				this.paddle.speed += 1;
				this.ball.speed += 1;
				this.round += 1;
			}
		}
		// Check to see if the paddle/AI has won the round.
		else if (this.paddle.score === rounds[this.round]) {
			this.over = true;
			setTimeout(function () { alert('Game Over!'); }, 1000);
		}
	},

	// Draw the objects to the canvas element
	draw: function () {
        // Clear the map
        if (this.playerPoly) {
            this.playerPoly.remove();
            this.paddlePoly.remove();
            this.ballPoly.remove();
        }
        

		// Draw the Player
		/*this.context.fillRect(
			this.player.x,
			this.player.y,
			this.player.width,
			this.player.height
        );*/
        this.playerPoly = L.rectangle([
            [this.player.y, this.player.x],
            [this.player.y + this.player.height, this.player.x + this.player.width]
        ], {
            color: "#ff7800",
            weight: 1
        }).addTo(this.map)

		// Draw the Paddle
		/*this.context.fillRect(
			this.paddle.x,
			this.paddle.y,
			this.paddle.width,
			this.paddle.height
        );*/
        this.paddlePoly = L.rectangle([
            [this.paddle.y, this.paddle.x],
            [this.paddle.y + this.paddle.height, this.paddle.x + this.paddle.width]
        ], {
            color: "#ff7800",
            weight: 1
        }).addTo(this.map)

		// Draw the Ball
		if (Pong._turnDelayIsOver.call(this)) {
			/*this.context.fillRect(
				this.ball.x,
				this.ball.y,
				this.ball.width,
				this.ball.height
            );*/
            this.ballPoly = L.rectangle([
                [this.ball.y, this.ball.x],
                [this.ball.y + this.ball.height, this.ball.x + this.ball.width]
            ], {
                color: "#ff7800",
                weight: 1
            }).addTo(this.map)
            console.log(this.ball.x, this.ball.y)
        }
	},

	loop: function () {
		Pong.update();
		Pong.draw();

		// If the game is not over, draw the next frame.
		if (!Pong.over) requestAnimationFrame(Pong.loop);
	},

	listen: function () {
		document.addEventListener('keydown', function (key) {
			// Handle the 'Press any key to begin' function and start the game.
			if (Pong.running === false) {
				Pong.running = true;
				window.requestAnimationFrame(Pong.loop);
			}

			// Handle up arrow and w key events
			if (key.keyCode === 38 || key.keyCode === 87) Pong.player.move = DIRECTION.UP;

			// Handle down arrow and s key events
			if (key.keyCode === 40 || key.keyCode === 83) Pong.player.move = DIRECTION.DOWN;
		});

		// Stop the player from moving when there are no keys being pressed.
		document.addEventListener('keyup', function (key) { Pong.player.move = DIRECTION.IDLE; });
	},

	// Reset the ball location, the player turns and set a delay before the next round begins.
	_resetTurn: function(victor, loser) {
		this.ball = Ball.new.call(this, this.ball.speed);
		this.turn = loser;
		this.timer = (new Date()).getTime();

		victor.score++;
	},

	// Wait for a delay to have passed after each turn.
	_turnDelayIsOver: function() {
		return ((new Date()).getTime() - this.timer >= 1000);
	},

	// Select a random color as the background of each level/round.
	_generateRoundColor: function () {
		var newColor = colors[Math.floor(Math.random() * colors.length)];
		if (newColor === this.color) return Pong._generateRoundColor();
		return newColor;
	}
};

var Pong = Object.assign({}, Game);
Pong.initialize(map);

var ballPoly = L.rectangle([
    [gameLeft, edgeTop],
    [gameLeft + ballSize, edgeTop - ballSize]
], {
    color: "#ff7800",
    weight: 1
}).addTo(map)