scoreDisplayLeft.setTooltipContent("To play this game, add startgame/ to the end of the url, or click the top circle");
scoreDisplayRight.setTooltipContent("To play this game, add startgame/ to the end of the url, or click the top circle");

infoTextLeft.setTooltipContent("Click here to start a game");
infoTextRight.setTooltipContent("Click here to start a game");

controlPaddleLeft.on('click', startNewGame);
controlPaddleRight.on('click', startNewGame);