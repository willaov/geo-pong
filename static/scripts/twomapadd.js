var myaccessToken = "pk.eyJ1Ijoid2lsbGFvdiIsImEiOiJjazYwd21vNjcwYnFkM25ueTB3dXBuOHAwIn0.shHDpzplHEF1sktnlDWLxw";
var accessToken = "pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw";

// Create Map
var leftmap = L.map('halfmaptopleft', {
    center: [0, 0],
    zoom: 1,
    zoomControl: false,
    doubleClickZoom: false
});
var rightmap = L.map('halfmaptopright', {
    center: [0, 0],
    zoom: 1,
    zoomControl: false,
    doubleClickZoom: false
});

// Add map tiles
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    autoPan: false,
    zoomSnap: 0,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: accessToken
}).addTo(leftmap);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    autoPan: false,
    zoomSnap: 0,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: accessToken
}).addTo(rightmap);

// Draw polygon
var leftPlayRectangle = L.rectangle([[0,0],[0,0]], {fill: false}).addTo(leftmap);
var rightPlayRectangle = L.rectangle([[0,0],[0,0]], {fill: false}).addTo(rightmap);

// Set form values
function setForm() {
    var bEdgeLeft = document.getElementById("bEdgeLeft");
    var lEdgeLeft = document.getElementById("lEdgeLeft");
    var bEdgeRight = document.getElementById("bEdgeRight");
    var lEdgeRight = document.getElementById("lEdgeRight");
    var height = document.getElementById("height");
    var width = document.getElementById("width");
    bEdgeLeft.value = leftPlayRectangle.getBounds().getSouth();
    lEdgeLeft.value = leftPlayRectangle.getBounds().getWest();
    bEdgeRight.value = rightPlayRectangle.getBounds().getSouth();
    lEdgeRight.value = rightPlayRectangle.getBounds().getWest();
    height.value = rightPlayRectangle.getBounds().getNorth() - rightPlayRectangle.getBounds().getSouth();
    width.value = (rightPlayRectangle.getBounds().getEast() - rightPlayRectangle.getBounds().getWest()) * 2;
}

// Set polygon coords
function onRightMapLeftClick(e) {
    var ne = rightPlayRectangle.getBounds().getNorthEast();
    var bounds = [e.latlng, ne];
    rightPlayRectangle.setBounds(bounds);
    if (rightPlayRectangle.getBounds().getSouth() < e.latlng.lat || rightPlayRectangle.getBounds().getWest() < e.latlng.lng) {
        rightPlayRectangle.setBounds([e.latlng, e.latlng]);
    }
    setForm();
}

function onRightMapRightClick(e) {
    var sw = rightPlayRectangle.getBounds().getSouthWest();
    var bounds = [sw, e.latlng];
    rightPlayRectangle.setBounds(bounds);
    if (rightPlayRectangle.getBounds().getNorth() > e.latlng.lat || rightPlayRectangle.getBounds().getEast() > e.latlng.lng) {
        rightPlayRectangle.setBounds([e.latlng, e.latlng]);
    }
    setForm();
}

function onLeftMapLeftClick(e) {
    var height = rightPlayRectangle.getBounds().getNorth() - rightPlayRectangle.getBounds().getSouth();
    var width = rightPlayRectangle.getBounds().getEast() - rightPlayRectangle.getBounds().getWest();
    var bounds = [[e.latlng.lat-height/2, e.latlng.lng-width/2], [e.latlng.lat+height/2, e.latlng.lng+width/2]]
    leftPlayRectangle.setBounds(bounds);

    height = height*getMercatorScaling(rightPlayRectangle.getBounds().getSouth(), leftPlayRectangle.getBounds().getSouth(), height)
    bounds = [[e.latlng.lat-height/2, e.latlng.lng-width/2], [e.latlng.lat+height/2, e.latlng.lng+width/2]]
    leftPlayRectangle.setBounds(bounds);
    setForm();
}

rightmap.on('dblclick', onRightMapLeftClick);
rightmap.on('contextmenu', onRightMapRightClick);
leftmap.on('dblclick', onLeftMapLeftClick);

// Center map on person
rightmap.locate({setView: true, maxZoom: 16});
leftmap.locate({setView: true, maxZoom: 16});
function onLocationError(e) {
    alert(e.message);
}
rightmap.on('locationerror', onLocationError);
leftmap.on('locationerror', onLocationError);

// Mercator Functions
function deg2rad(degrees)
{
    var pi = Math.PI;
    return degrees * (pi/180);
}

function getMercatorCoord(lat)
{
    return Math.log(Math.tan(Math.PI/4 + lat/2));
}

function getMercatorScaling(l1, l2, height)
{
    l1 = deg2rad(l1)
    l1top = l1 + deg2rad(height)
    l2 = deg2rad(l2)
    l2top = l2 + deg2rad(height)

    y1 = getMercatorCoord(l1)
    y1top = getMercatorCoord(l1top)
    y2 = getMercatorCoord(l2)
    y2top = getMercatorCoord(l2top)

    deltay1 = Math.abs(y1top-y1)
    deltay2 = Math.abs(y2top-y2)
    return deltay1/deltay2
}