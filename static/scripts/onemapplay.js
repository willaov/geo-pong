// Control everything
function movePaddle(e) {
    if (controlKey === null && takingControl) {
        if (e.latlng.lng <= centerPoint[1]) {
            if (confirm("Do you want to take control of the West paddle?")) {
                getText('controlLeftPaddle/',  function(err, data) {
                    if (err != null) {
                        console.error(err);
                    } else {
                        controlKey = data
                        console.log(controlKey)
                    }
                });
                sendData = e.latlng
                sendData.key = controlKey
                sendJSONVerify('moveLeftPaddle/', sendData);
            }
            else {
                alert("You can only control the paddle for the half you're in")
            }
        }
        else {
            if (confirm("Do you want to take control of the East paddle?")) {
                getText('controlRightPaddle/',  function(err, data) {
                    if (err != null) {
                        console.error(err);
                    } else {
                        controlKey = data
                        console.log(controlKey)
                    }
                });
                sendData = e.latlng
                sendData.key = controlKey
                sendJSONVerify('moveRightPaddle/', sendData);
            }
            else {
                alert("You can only control the paddle for the half you're in")
            }
        }
    }
    else {
        if (e.latlng.lng <= centerPoint[1]) {
            sendData = e.latlng
            sendData.key = controlKey
            sendJSONVerify('moveLeftPaddle/', sendData);
        }
        else {
            sendData = e.latlng
            sendData.key = controlKey
            sendJSONVerify('moveRightPaddle/', sendData);
        }
    }
}

function onMapClick(e) {
    takingControl = true;
    movePaddle(e);
}

// Number of updated since game end
var endCount = 0;

function onLocateClick() {
    if (endCount > 1) {
        if (confirm("Start new game?")) {
            startNewGame();
        }
    }
    takingControl = true
    map.locate();
}

function locateUpdate() {
    try {
        map.locate();
    }
    catch (e) {
        console.err(e);
        setTimeout(locateUpdate, 1000/pollFrequency);
    }
}

function onLocationFound(e) {
    console.log("Moving to location")
    movePaddle(e);
    if (controlKey !== null || takingControl === true) {
        setTimeout(locateUpdate, 1000/pollFrequency);
    }
    takingControl = false;
}

map.on('locationfound', onLocationFound);

function onLocationError(e) {
    alert(e.message);
}

map.on('locationerror', onLocationError);

var endCount = 0;
function updateGame() {
    getJSON('gamestate/',  function(err, data) {

        if (err != null) {
            console.error(err);
        } else {
            // Move all items to current location
            leftPaddle.setBounds(data.leftPlayer.bounds);
            rightPaddle.setBounds(data.rightPlayer.bounds);
            if (ballTrails) {
                ball = L.rectangle(data.ball.bounds, {
                    color: "#ff7800",
                    weight: 1,
                    fillOpacity: 1
                }).addTo(map)
            }
            else {
                ball.setBounds(data.ball.bounds);
            }
            if (!data.over) {
                scoreDisplay.setTooltipContent(`Score: ${data.leftPlayer.score} - ${data.rightPlayer.score}`);
                endCount = 0;
            }
            else {
                scoreDisplay.setTooltipContent(`Victory to ${data.leftPlayer.score > data.rightPlayer.score ? "West Team" : "East Team"}<br>\
                                                Click the circle at the top to play again`);
                endCount++;
            }
        }
    });
}

setInterval(updateGame, 1000/updateFrequency)

controlPaddle.on('click', onLocateClick);
