from flask import Flask, Request, render_template, request, url_for, abort
from flask import redirect as redirect_default
import json
import string
import random
from subprocess import check_output, call, CalledProcessError
import numpy as np
import os
from time import sleep
from time import time as currentTime


class R(Request):
    # Whitelist your SRCF and/or custom domains to access the site via proxy.
    trusted_hosts = {"waov2.user.srcf.net"}


def redirect(*args, **kwargs):
    res = redirect_default(*args, **kwargs)
    res.autocorrect_location_header = False
    return res


app = Flask(__name__)
app.request_class = R


def getPid(identifier):
    return int(check_output(["pgrep", "-f", "geo-pong-{}".format(identifier)]))


def dispatchThread(region, gamekey, time=None, updateFrequency=None, score=None):
    command = ["screen", "-d", "-m", "-S", "geo-pong-{}".format(gamekey), "./game.py", region, gamekey]
    if time:
        command.append("-t")
        command.append(str(time))
    if updateFrequency:
        command.append("-f")
        command.append(str(updateFrequency))
    if score:
        command.append("-s")
        command.append(str(score))
    print(command)
    return call(command)


def getMercatorCoord(lat):
    return np.log(np.tan(np.pi/4 + lat/2))


def getMercatorScaling(l1, l2, height):
    l1 = np.deg2rad(l1)
    l1top = l1 + np.deg2rad(height)
    l2 = np.deg2rad(l2)
    l2top = l2 + np.deg2rad(height)

    y1 = getMercatorCoord(l1)
    y1top = getMercatorCoord(l1top)
    y2 = getMercatorCoord(l2)
    y2top = getMercatorCoord(l2top)

    deltay1 = abs(y1top-y1)
    deltay2 = abs(y2top-y2)
    return deltay1/deltay2


class Region():
    """ Class to represent one map sector
    """
    def __init__(self, location, bEdge, lEdge, height, width):
        bEdge = float(bEdge)
        lEdge = float(lEdge)
        height = float(height)
        width = float(width)
        self.location = location
        self.bounds = [
            [bEdge, lEdge],
            [bEdge + height, lEdge + width]
        ]
        self.sector = [
            [bEdge, lEdge],
            [bEdge, lEdge + width],
            [bEdge + height, lEdge + width],
            [bEdge + height, lEdge]
        ]
        self.bEdge = bEdge
        self.lEdge = lEdge
        self.height = height
        self.width = width
        self.centerPoint = [bEdge + height/2, lEdge + width/2]


class Regions():
    """ Class to represent the two map sectors which are being used
    """
    def __init__(self, location, bEdgeLeft, lEdgeLeft, bEdgeRight, lEdgeRight, height, width):
        self.location = location
        # Scale left map to make it same height as right one, due to Mercator
        self.leftYScaling = getMercatorScaling(bEdgeRight, bEdgeLeft, height)
        # self.leftYScaling = 1.0
        self.leftBounds = [
            [bEdgeLeft, lEdgeLeft],
            [bEdgeLeft + height*self.leftYScaling, lEdgeLeft + width/2]
        ]
        self.leftSector = [
            [bEdgeLeft, lEdgeLeft],
            [bEdgeLeft, lEdgeLeft + width/2],
            [bEdgeLeft + height*self.leftYScaling, lEdgeLeft + width/2],
            [bEdgeLeft + height*self.leftYScaling, lEdgeLeft]
        ]
        self.rightBounds = [
            [bEdgeRight, lEdgeRight],
            [bEdgeRight + height, lEdgeRight + width/2]
        ]
        self.rightSector = [
            [bEdgeRight, lEdgeRight],
            [bEdgeRight, lEdgeRight + width/2],
            [bEdgeRight + height, lEdgeRight + width/2],
            [bEdgeRight + height, lEdgeRight]
        ]
        self.bEdgeLeft = bEdgeLeft
        self.lEdgeLeft = lEdgeLeft
        self.bEdgeRight = bEdgeRight
        self.lEdgeRight = lEdgeRight
        self.height = height
        self.lheight = self.height*self.leftYScaling
        self.width = width
        self.centerPointLeft = [bEdgeLeft + self.lheight/2, lEdgeLeft + width/4]
        self.centerPointRight = [bEdgeRight + height/2, lEdgeRight + width/4]


officialGames = [
    "cambridge",
    "cambridgesmall",
    "oxbridge",
    "lammas"
    ]


class GameRegions(dict):
    @property
    def officialGames(self):
        return officialGames

    @property
    def allGames(self):
        allFiles = os.listdir()
        allGames = []
        for file in allFiles:
            if os.path.isfile(file):
                if ".json" in file:
                    allGames.append(str(file).replace(".json", ''))
        return allGames

    def __getitem__(self, key):
        try:
            item = json.load(open(key + ".json"))
        except (json.decoder.JSONDecodeError, FileNotFoundError):
            raise KeyError
        try:
            return Regions(**item)
        except TypeError:
            return Region(**item)

    def __setitem__(self, key, value):
        if key != value["location"]:
            raise KeyError("The key must match the region location")
        try:
            open(key + ".json")
            raise KeyError("This location already exists")
        except FileNotFoundError:
            pass
        region = value
        json.dump(region, open(region["location"] + ".json", "w"))


gameRegions = GameRegions()

zoomLevel = 13


def getRandomString(length):
    letters = string.ascii_letters
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str


@app.route("/")
def root():
    return render_template(
        "root.html",
        officialGames=gameRegions.officialGames,
        allGames=gameRegions.allGames
        )


@app.route("/addone/<location>/", methods=["GET"])
def addone(location):
    try:
        gameRegions[location]
        return "That location already exists"
    except KeyError:
        return render_template("addone.html")


@app.route("/addone/<location>/", methods=["POST"])
def addonePost(location):
    try:
        formDict = dict(request.form)
        # Make all values into floats, rather than arrays
        for key in formDict:
            if isinstance(formDict[key], list):
                formDict[key] = formDict[key][0]
            formDict[key] = float(formDict[key])
        formDict["location"] = location
        gameRegions[formDict["location"]] = formDict
        return redirect(url_for('gameView', location=location))
    except KeyError as e:
        print(e)
        return str(e)


@app.route("/addtwo/<location>/", methods=["GET"])
def addtwo(location):
    try:
        gameRegions[location]
        return "That location already exists"
    except KeyError:
        return render_template("addtwo.html")


@app.route("/addtwo/<location>/", methods=["POST"])
def addtwoPost(location):
    try:
        formDict = dict(request.form)
        # Make all values into floats, rather than arrays
        for key in formDict:
            if isinstance(formDict[key], list):
                formDict[key] = formDict[key][0]
            formDict[key] = float(formDict[key])
        formDict["location"] = location
        gameRegions[formDict["location"]] = formDict
        return redirect(url_for('gameView', location=location))
    except KeyError as e:
        print(e)
        return str(e)


@app.route("/<location>/")
def gameView(location):
    debug = bool(request.args.get("debug"))
    ballTrails = bool(request.args.get("trails"))
    try:
        region = gameRegions[location]
        if isinstance(region, Region):
            return render_template(
                "onemap.html",
                playRegion=region.sector,
                bounds=region.bounds,
                centerPoint=region.centerPoint,
                zoomLevel=zoomLevel,
                debug=debug,
                ballTrails=ballTrails,
                view=True
            )
        elif isinstance(region, Regions):
            return render_template(
                "twomaps.html",
                playRegionLeft=region.leftSector,
                playRegionRight=region.rightSector,
                boundsLeft=region.leftBounds,
                boundsRight=region.rightBounds,
                centerPointLeft=region.centerPointLeft,
                centerPointRight=region.centerPointRight,
                leftYScaling=region.leftYScaling,
                zoomLevel=zoomLevel,
                debug=debug,
                ballTrails=ballTrails,
                view=True
            )
        else:
            abort(500)
    except KeyError:
        abort(404)


@app.route("/<location>/<gamekey>/")
def game(location, gamekey):
    debug = bool(request.args.get("debug"))
    ballTrails = bool(request.args.get("trails"))
    if not os.path.isfile(gamekey + "/" + location + ".json"):
        abort(404)
    try:
        region = gameRegions[location]
        if isinstance(region, Region):
            return render_template(
                "onemap.html",
                playRegion=region.sector,
                bounds=region.bounds,
                centerPoint=region.centerPoint,
                zoomLevel=zoomLevel,
                debug=debug,
                ballTrails=ballTrails
            )
        elif isinstance(region, Regions):
            return render_template(
                "twomaps.html",
                playRegionLeft=region.leftSector,
                playRegionRight=region.rightSector,
                boundsLeft=region.leftBounds,
                boundsRight=region.rightBounds,
                centerPointLeft=region.centerPointLeft,
                centerPointRight=region.centerPointRight,
                leftYScaling=region.leftYScaling,
                zoomLevel=zoomLevel,
                debug=debug,
                ballTrails=ballTrails
            )
        else:
            abort(500)
    except (KeyError, AssertionError):
        abort(404)


@app.route("/<location>/<gamekey>/gamestate/")
def gameState(location, gamekey):
    try:
        gameState = json.load(open(gamekey + "/gameState.json"))
        return json.dumps(gameState)
    except (json.decoder.JSONDecodeError, FileNotFoundError):
        return ""


@app.route("/<location>/<gamekey>/controlLeftPaddle/")
def controlLeftPaddle(location, gamekey):
    if not os.path.isfile(gamekey + "/" + location + ".json"):
        abort(404)
    try:
        controlKeys = json.load(open(gamekey + "/controlKeys.json"))
    except (FileNotFoundError, json.decoder.JSONDecodeError):
        controlKeys = {}
    controlKeys[location + "Left"] = getRandomString(10)
    json.dump(controlKeys, open(gamekey + "/controlKeys.json", "w"))
    return controlKeys[location + "Left"]


@app.route("/<location>/<gamekey>/controlRightPaddle/")
def controlRightPaddle(location, gamekey):
    if not os.path.isfile(gamekey + "/" + location + ".json"):
        abort(404)
    try:
        controlKeys = json.load(open(gamekey + "/controlKeys.json"))
    except (FileNotFoundError, json.decoder.JSONDecodeError):
        controlKeys = {}
    controlKeys[location + "Right"] = getRandomString(10)
    json.dump(controlKeys, open(gamekey + "/controlKeys.json", "w"))
    return controlKeys[location + "Right"]


@app.route("/<location>/<gamekey>/moveLeftPaddle/", methods=["POST"])
def moveLeftPaddle(location, gamekey):
    content = request.json
    controlKeys = json.load(open(gamekey + "/controlKeys.json"))
    if content["key"] == controlKeys[location + "Left"]:
        json.dump(content, open(gamekey + "/leftPaddle.json", "w"))
        return "Success"
    else:
        return "Not Authorised"


@app.route("/<location>/<gamekey>/moveRightPaddle/", methods=["POST"])
def moveRightPaddle(location, gamekey):
    content = request.json
    controlKeys = json.load(open(gamekey + "/controlKeys.json"))
    if content["key"] == controlKeys[location + "Right"]:
        json.dump(content, open(gamekey + "/rightPaddle.json", "w"))
        print("Success")
        return "Success"
    else:
        print("Not Authorised")
        return "Not Authorised"


@app.route("/<location>/startgame/", methods=["GET"])
def startGameNoKey(location):
    gamekey = getRandomString(10)
    return redirect(url_for("startGameSetup", location=location, gamekey=gamekey))


@app.route("/<location>/<gamekey>/startgame/", methods=["GET"])
def startGameSetup(location, gamekey):
    try:
        if getPid(gamekey):
            return "Game Already In Progress"
    except CalledProcessError:
        pass
    return render_template("startGame.html")


@app.route("/<location>/<gamekey>/startgame/", methods=["POST"])
def startGame(location, gamekey):
    time = request.form.get("time")
    updateFrequency = request.form.get("updateFrequency")
    score = request.form.get("score")
    try:
        if getPid(gamekey):
            return "Game Already In Progress"
    except CalledProcessError:
        pass
    if dispatchThread(location, gamekey, time=time, updateFrequency=updateFrequency, score=score) == 0:
        while not os.path.isfile(gamekey + "/" + location + ".json"):
            sleep(0.5)
        return redirect(url_for("game", location=location, gamekey=gamekey))
    else:
        return "Failed"


@app.route("/cleanup/")
def cleanup():
    # Removes games that haven't been used for 30mins
    removedDirs = []
    for thing in os.listdir("."):
        if os.path.isdir(thing):
            if len(thing) == 10:
                if os.path.isfile(os.path.join(thing, "gameState.json")):
                    fp = os.path.join(thing, "gameState.json")
                    if os.path.getmtime(fp) < currentTime() - 1800:
                        for root, dirs, files in os.walk(thing):
                            for name in files:
                                os.remove(os.path.join(root, name))
                        os.removedirs(thing)
                        removedDirs.append(thing)
    return "Done, removed {}".format(removedDirs)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
